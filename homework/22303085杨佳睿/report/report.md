<font size =6>**操作系统原理 实验四** `</font>`

## 个人信息

（此部分需补充完整）

【院系】计算机学院

【专业】计算机科学与技术

【学号】22303085

【姓名】杨佳睿

## 实验题目

从实模式到保护模式

## 实验目的

1. 学习C代码变成C程序的过程。
2. 学习C/C++项目的组织方法。
3. 学习makefile的使用。
4. 学习C和汇编混合编程。
5. 学习保护模式中断处理机制。
6. 学习8259A可编程中断处理部件。
7. 学习时钟中断的处理。

## 实验要求

1. 学习混合编程基本思路
2. 使用C/C++编写内核
3. 学习对中断的处理
4. 实现时钟中断
5. 撰写实验报告

## 实验方案

### Assignment 1 混合编程

掌握C代码变成C程序的过程，即代码编译的四个阶段：

* **预处理** 。处理宏定义，如 `#include`, `#define`, `#ifndef`等，生成预处理文件，一般以 `.i`为后缀。
* **编译** 。将预处理文件转换成汇编代码文件，一般以 `.S`为后缀。
* **汇编** 。将汇编代码文件文件转换成可重定位文件，一般以 `.o`为后缀。
* **链接** 。将多个可重定位文件链接生成可执行文件，一般以 `.o`为后缀。

学习make编译工具，使用其编译C/C++项目

复现Example 1，结合具体的代码说明C代码调用汇编函数的语法和汇编代码调用C函数的语法

### Assignment 2 使用C/C++来编写内核

复现Example 2，在进入 `setup_kernel`函数后，将输出 Hello World 改为输出我的学号

### Assignment 3中断的处理

复现Example 3，你可以更改Example中默认的中断处理函数为你编写的函数，然后触发之

### Assignment 4 时钟中断

包括：硬件或虚拟机配置方法、软件工具与作用、方案的思想、相关原理、程序流程、算法和数据结构、程序关键模块，结合代码与程序中的位置位置进行解释。不得抄袭，否则按作弊处理。

## 实验过程

### 1.复现Example1

首先在文件 `c_func.c`中定义C函数 `function_from_C`。

```
#include <stdio.h>

void function_from_C() {
    printf("This is a function from C.\n");
}
```

然后在文件 `cpp_func.cpp`中定义C++函数 `function_from_CPP`。

```
#include <iostream>

extern "C" void function_from_CPP() {
    std::cout << "This is a function from C++." << std::endl;
}
```

接着在文件 `asm_func.asm`中定义汇编函数 `function_from_asm`，在 `function_from_asm`中调用 `function_from_C`和 `function_from_CPP`。

```
[bits 32]
global function_from_asm
extern function_from_C
extern function_from_CPP

function_from_asm:
    call function_from_C
    call function_from_CPP
    ret
```

在文件 `main.cpp`中调用汇编函数 `function_from_asm`。

```
#include <iostream>

extern "C" void function_from_asm();

int main() {
    std::cout << "Call function from assembly." << std::endl;
    function_from_asm();
    std::cout << "Done." << std::endl;
```

接着将这4个文件统一编译成可重定位文件即 `.o`文件，然后将这些 `.o`文件链接成一个可执行文件，编译命令分别如下。

```
gcc -o c_func.o -m32 -c c_func.c
g++ -o cpp_func.o -m32 -c cpp_func.cpp 
g++ -o main.o -m32 -c main.cpp
nasm -o asm_func.o -f elf32 asm_func.asm
g++ -o main.out main.o c_func.o cpp_func.o asm_func.o -m32
```

其中，`-f elf32`指定了nasm编译生成的文件格式是 `ELF32`文件格式，`ELF`文件格式也就是Linux下的 `.o`文件的文件格式。

最后执行 `main.out`

运行结果：

![1713683621829](image/report/1713683621829.png)

`extern`作用：当我们需要在汇编代码中使用C的函数 `function_from_C`时，我们需要在汇编代码中声明这个函数来自于外部。

如 `extern function_from_C`

但是，如果我们需要在汇编代码中使用来自C++的函数 `function_from_CPP`时，我们需要现在C++代码的函数定义前加上 `extern "C"`。因为C++支持函数重载，为了区别同名的重载函数，C++在编译时会进行名字修饰。也就是说，`function_from_CPP`编译后的标号不再是 `function_from_CPP`，而是要带上额外的信息。而C代码编译后的标号还是原来的函数名。因此，`extern "C"`目的是告诉编译器按C代码的规则编译，不进行名字修饰。

如 `extern "C" void function_from_CPP()`

### 2.复现Example2

在Example 2中，我们在bootloader中加载操作系统内核到地址0x20000，然后跳转到0x20000。内核接管控制权后，输出“Hello World”。

1. 在bootloader的最后加上读取内核的代码
2. 在 `include/boot.inc`下定义常量
3. 编写内核：

* 在 `src/boot/entry.asm`下定义内核进入点。

```
extern setup_kernel
enter_kernel:
    jmp setup_kernel
```

* 在链接阶段巧妙地将entry.asm的代码放在内核代码的最开始部份，使得bootloader在执行跳转到 `0x20000`后，即内核代码的起始指令，执行的第一条指令是 `jmp setup_kernel`。在 `jmp`指令执行后，我们便跳转到使用C++编写的函数 `setup_kernel`
* 此后，我们便可以使用C++来写内核了：

  `setup_kernel`的定义在文件 `src/kernel/setup.cpp`中，内容如下：

```
#include "asm_utils.h"

extern "C" void setup_kernel()
{
    asm_hello_world();
    while(1) {

    }
}
```

   4.编写汇编代码asm_utils.asm

   5.统一在文件 `include/asm_utils.h`中声明所有的汇编函数

   6.编译MBR、bootloader

   7.编译内核

   8.编译 `setup.cpp`

   9.链接生成的可重定位文件为两个文件：只包含代码的文件 `kernel.bin`，可执行文件 `kernel.o`

   10.将 `mbr.bin bootloader.bin kernel.bin`写入硬盘

   11.在 `run`目录下，启动qemu

复现截图：

![1713689774466](image/report/1713689774466.png)

在进入 `setup_kernel`函数后输出学号：

修改的asm_utils.asm代码如下：

```
[bits 32]

global asm_hello_world

asm_hello_world:
    push eax
    xor eax, eax

    mov ah, 0x03 ;青色
    mov al, '2'
    mov [gs:2 * 0], ax

    mov al, '2'
    mov [gs:2 * 1], ax

    mov al, '3'
    mov [gs:2 * 2], ax

    mov al, '0'
    mov [gs:2 * 3], ax

    mov al, '3'
    mov [gs:2 * 4], ax

    mov al, '0'
    mov [gs:2 * 5], ax

    mov al, '8'
    mov [gs:2 * 6], ax

    mov al, '5'
    mov [gs:2 * 7], ax

    pop eax
    ret
```

编译、启动qemu截图如下：

![1713690099111](image/report/1713690099111.png)

### 3.复现Example 3 初始化IDT：

要做的事情有三件：

* 确定IDT的地址。
* 定义中断默认处理函数。
* 初始化256个中断描述符。

1.确定IDT的地址。函数 `InterruptManager::initialize` 用来初始化IDT和IDTR，以及256个中断描述符

2.定义中断处理函数： `asm_interrupt_empty_handler` 它首先关中断，然后输出提示字符

3.定义并初始化中断处理器

在qemu的debug模式下加载运行，在gdb下使用 `x/256gx 0x8880`命令可以查看我们是否已经放入默认的中断描述符

调试结果如下所示：

![1713757255149](image/report/1713757255149.png)

可以看出结果符合我们的预期，这里为什么是0x0028e000020016c而不是0x0028e000020016b，有可能是我们的初始地址与教程的不一样造成的。

初始化IDT后，我们尝试触发除0异常来验证 `asm_unhandled_interrupt`是否可以正常工作。修改 `setup.cpp`即可

编译运行结果如下：

![1713756173042](image/report/1713756173042.png)

### 4.复现Eample4，输出学号和英文昵称

具体步骤：

1. 初始化8259A芯片
2. 处理时钟中断，我们处理的时钟中断是主片的IRQ0中断，具体来说：
3. 编写中断处理函数。
4. 设置主片IRQ0中断对应的中断描述符。
5. 开启时钟中断
6. 开中断



此时，我们需要对屏幕进行输出，之前我们只是单纯地往显存地址上赋值来显示字符。但是这样做并不太方便，我们希望能够像printf和putchar这样的函数来调用。因此，我们下面简单封装一个能够处理屏幕输出的类 `STDIO`，声明放置在文件 `include/stdio.h`中，如下所示。

```
#ifndef STDIO_H
#define STDIO_H

#include "os_type.h"

class STDIO
{
private:
    uint8 *screen;

public:
    STDIO();
    // 初始化函数
    void initialize();
    // 打印字符c，颜色color到位置(x,y)
    void print(uint x, uint y, uint8 c, uint8 color);
    // 打印字符c，颜色color到光标位置
    void print(uint8 c, uint8 color);
    // 打印字符c，颜色默认到光标位置
    void print(uint8 c);
    // 移动光标到一维位置
    void moveCursor(uint position);
    // 移动光标到二维位置
    void moveCursor(uint x, uint y);
    // 获取光标位置
    uint getCursor();

public:
    // 滚屏
    void rollUp();
};

#endif
```

在 `STDIO::print`的实现中，我们向光标处写入了字符并移动光标到下一个位置。

接下来，我们定义中断处理函数 `c_time_interrupt_handler`，由于需要像跑马灯一样的输出自己的学号，因此修改后的代码如下：

![1713791061053](image/report/1713791061053.png)

跑马灯效果如下：

![1713790915643](image/report/1713790915643.png)

包括：主要工具安装使用过程及截图结果、程序过程中的操作步骤、测试数据、输入及输出说明、遇到的问题及解决情况、关键功能或操作的截图结果。不得抄袭，否则按作弊处理。

## 实验总结

这次的实验内容非常的多，从C++和汇编语言混合编程，到使用C++来编写内核，再到保护模式下的中断和时钟中断的处理，可以说是在各个方面都有了非常多全新的了解和认识。首先是混合编程，我第一次知道原来C++中可以调用汇编函数，汇编代码中也可以调用C++的函数，只需要在调用前增加相应的extern声明这个函数来自外部即可；其次使用C++来编写内核，由于在之前的实验中，我们遇到的问题都是用汇编语言解决的，那这一次用C++语言来编写内核，无论是实现方式还有指令方面都是完全不一样的，这促使我们从不同的方式和角度去看待一个相同的问题，有助于我从两个方面了解内核的基本原理及其编写过程，非常有利于我自身能力的提高以及对OS相关知识的了解。当然，C++也只能完成大部分的内核编写工作，汇编代码仍需要为我们提供汇编指令的封装，这样的目的是不使用内联汇编，所以，掌握两门编程语言，对于我们解决问题会非常有帮助，毕竟技多不压身，这就像老练娴熟的技术工人总能懂得使用不同的工具一起去解决问题，这样往往能得到事半功倍的效果；此外，在学习保护模式中断处理机制和8259A可编程中断处理部件时，我了解了操作系统是如何管理和响应中断的，以及中断处理的过程。这为我理解操作系统的核心功能打下了坚实的基础。最后，在时钟中断处理的实践中，通过对中断处理函数的学习理解，我成功实现了跑马灯的效果，看到自己的学号和英文昵称依次打印在屏幕上时，我难掩内心的激动与喜悦！相信在未来的实验中，我肯定会不畏困难，勇往直前，在OS领域收获更多的成就！

每人必需写一段，文字不少于500字，可以写心得体会、问题讨论与思考、新的设想、感言总结或提出建议等等。不得抄袭，否则按作弊处理。

## 参考文献

（如有要列出，包括网上资源）
